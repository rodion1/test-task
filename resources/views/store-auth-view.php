<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <style>

    *,
    *:before,
    *:after {
      box-sizing: border-box;
    }

    html,
    body {
      padding: 0;
      margin: 0;
    }

    html {
      position: relative;
      font-size: 62.5%;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      text-size-adjust: 100%;
      text-rendering: optimizeLegibility;
    }

    body {
      font-size: 1.5rem;
      line-height: 2rem;
      text-transform: none;
      letter-spacing: normal;
      font-weight: 400;
      color: #212b36;
      font-family: -apple-system,BlinkMacSystemFont,San Francisco,Roboto,Segoe UI,Helvetica Neue,sans-serif;
    }

    form {
      max-width: 600px;
      margin: 0 auto;
    }

    button {
      position: relative;
      display: -webkit-inline-box;
      display: -ms-inline-flexbox;
      display: inline-flex;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      justify-content: center;
      min-height: 3.6rem;
      min-width: 3.6rem;
      margin: 0;
      padding: 0.7rem 1.6rem;
      border-radius: 3px;
      line-height: 1;
      text-align: center;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      text-decoration: none;
      transition-property: background, border, box-shadow;
      transition-duration: 200ms;
      transition-timing-function: cubic-bezier(0.64, 0, 0.35, 1);
      background: linear-gradient(180deg,#00b765,#00a359);
      border-color: #007a43;
      box-shadow: inset 0 1px 0 0 #00bc67, 0 1px 0 0 rgba(22,29,37,.05), 0 0 0 0 transparent;
      color: #fff;
      fill: #fff;
      width: 100%;
      font-size: 1.6rem;
    }

    .input-field {
      font-size: 1.6rem;
      font-weight: 400;
      line-height: 2.4rem;
      text-transform: none;
      letter-spacing: normal;
      position: relative;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-align: center;
      align-items: center;
      color: #919eab;
      cursor: text;
      background: #fff;
      border: .1rem solid #c4cdd5;
      box-shadow: inset 0 1px 0 0 rgba(99,115,129,.05);
      border-radius: 3px;
      will-change: box-shadow,border-color;
      transition-property: box-shadow,border-color;
      transition-duration: .2s;
      transition-timing-function: cubic-bezier(.64,0,.35,1);
      margin-bottom: 10px;
    }

    input {
      font-size: 1.6rem;
      font-weight: 400;
      line-height: 2.4rem;
      text-transform: none;
      letter-spacing: normal;
      position: relative;
      z-index: 20;
      display: block;
      -ms-flex: 1 1 0%;
      flex: 1 1 0%;
      width: 100%;
      min-width: 0;
      min-height: 3.6rem;
      margin: 0;
      padding: .5rem 1.2rem;
      background: none;
      border: .1rem solid transparent;
      font-family: inherit;
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
    }
  </style>

</head>
<body>

  <form method="get" action="/">

    <h3>Enter your shop domain to log in or install this app</h3>

    <div class="input-field">
      <input type="text" name="shop" placeholder="example.myshopify.com">
    </div>
    <div>
      <button type="submit">Login</button>
    </div>

  </form>

</body>
</html>