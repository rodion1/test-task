<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo htmlentities(url('assets/css/styles.css'), ENT_QUOTES, false) ;?>">
</head>
<body>
<div class="order_stats_container">
    <h2>Your Country Order Stats</h2>
    <div class="inner_container">
        <?php if(!is_null($data['userCountry'])) { ?>
            <?php if ($data['countByCountry'] != 0) { ?>
                <p>Country: <?php echo $data['userCountry']; ?></p>
                <p>Your country orders count: <?php echo $data['countByCountry']; ?> (<?php echo $data['countryOrdersPercent'];?>% of all orders)</p>
                <p>Total orders count: <?php echo $data['countAll']; ?></p>
            <?php } else { ?>
                <p>Country: <?php echo $data['userCountry']; ?></p>
                <p>There are no orders from your country yet</p>
                <p>Why won't you buy something and be the first?:)</p>
            <?php } ?>
        <?php } else { ?>
                <p>Sorry - country is not defined :(</p>
        <?php } ?>
    </div>
</div>
</body>
</html>