<!DOCTYPE html>
<html>
<head>

    <?php if (!empty($redirect)): ?>

        <script type="text/javascript">
            (function () {

                try {
                    if (window.self === window.top) {
                        window.location.href = <?php echo json_encode($redirect);?>;
                    }
                } catch (e) {
                }

                window.top.location.href = <?php echo json_encode($redirect);?>;
            })();

        </script>
    <?php endif; ?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
</html>