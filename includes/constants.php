<?php

define('DS', DIRECTORY_SEPARATOR);
define('ABS_PATH', realpath(__DIR__ . DS . '..' . DS ) . DS );
define('VERSION', '1.0.0');
define('DEBUG', env('APP_DEBUG', false));