<?php

return [
    'api_key'         => env('API_KEY', null),
    'secret_key'      => env('SECRET_KEY', null),
    'refresh_token'   => env('REFRESH_TOKEN', null),
    'permissions'     => [
        'read_orders'
    ]
];
