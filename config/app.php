<?php

return [
    'env'             => DEBUG ? 'development' : 'production',
    'key'             => env('APP_KEY'),
    'cipher'          => 'AES-256-CBC',
    'locale'          => env('APP_LOCALE', 'en'),
    'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),
    'url'             => rtrim(env('SERVER_URL', null), '/')
];
