<?php

namespace App\Http\Middleware;

use App\Models\Store;
use App\Utils\Shopify\Api;
use Closure;
use Illuminate\Http\Request;

class WebhooksMiddleware {

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var Api
     */
    private $shopify;

    private $data;

    public function __construct( Api $shopify ) {
        $this->shopify = $shopify;
    }

    private function isValidRequest(): bool {

        $secretKey = config('shopify.secret_key');

        $hmac = $this->request->header('x-shopify-hmac-sha256', '');

        return $hmac === base64_encode( hash_hmac('sha256', $this->data, $secretKey, true ) );
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next ) {

        $this->request = $request;
        $this->data = file_get_contents('php://input');

        if( ! $this->isValidRequest() ) {
            die();
        }

        if ( ! $storeDomain = $this->request->header('x-shopify-shop-domain', null ) ) {
            die();
        }

        try {

            /**
             * @var $store Store
             */
            $store = Store::where('storeDomain', $storeDomain)->firstOrFail();
            if ( ! $store->isActive() ) {
                die();
            }

        } catch ( \Exception $e ) {
            die();
        }

        $this->shopify->setMyshopifyDomain($store->storeDomain);
        $this->shopify->setAccessToken($store->storeKey);

        $this->request->attributes->add( [
            'data' => @json_decode($this->data, true),
            'store' => $store
        ] );

        return $next($this->request);
    }
}
