<?php

namespace App\Http\Middleware;

use App\Models\Store;
use App\Utils\Shopify\Api;
use Closure;
use Illuminate\Http\Request;

class ProxyMiddleware {

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;


    /**
     * @var Api
     */
    private $shopify;


    /**
     * JwtMiddleware constructor.
     * @param Api $shopify
     */
    public function __construct(Api $shopify) {
        $this->shopify = $shopify;
    }

    public function isValidRequest(): bool {

        $query = $this->request->all();

        if (!isset($query['timestamp']) or !isset($query['signature'])) {
            return false;
        }

        $seconds_in_a_day = 24 * 60 * 60;
        $older_than_a_day = $query['timestamp'] < (time() - $seconds_in_a_day);

        if ($older_than_a_day) {
            return false;
        }

        $signature = $query['signature'];

        $dataString = array();

        foreach ($query as $key => $value) {

            if ( $key === 'signature' ) {
                continue;
            }

            $key = str_replace('=', '%3D', $key);
            $key = str_replace('&', '%26', $key);
            $key = str_replace('%', '%25', $key);
            $value = str_replace('&', '%26', $value);
            $value = str_replace('%', '%25', $value);
            $dataString[] = $key . '=' . $value;
        }

        sort($dataString);

        $string = implode("", $dataString);

        return (hash_hmac('sha256', $string, config('shopify.secret_key')) === $signature);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {

        $this->request = $request;

        if( $this->isValidRequest() ) {

            $shop = $this->request->get('shop');

            try {

                /**
                 * @var $store Store
                 */
                $store = Store::where('storeDomain', $shop)
                    ->firstOrFail();

                if ($store->isActive()) {

                    $this->shopify->setMyshopifyDomain($shop);
                    $this->shopify->setAccessToken($store->storeKey);

                    $this->request->attributes->add([
                        'store' => $store
                    ]);

                    return $next($this->request);
                }

                return redirect("https://{$shop}");

            } catch (\Exception $e) {}
        }

        return redirect("https://shopify.com");
    }
}
