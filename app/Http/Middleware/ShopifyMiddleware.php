<?php

namespace App\Http\Middleware;

use App\Services\ShopifyWebhooksService;
use App\Utils\Shopify\Api;
use App\Utils\Shopify\Helper\OAuthHelper;
use App\Models\Store;
use Closure;
use Illuminate\Http\Request;

class ShopifyMiddleware {

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var OAuthHelper
     */
    private $auth;

    /**
     * @var Api
     */
    private $shopify;

    /**
     * @var ShopifyWebhooksService
     */
    private $shopifyWebhooksService;



    /**
     * ShopifyMiddleware constructor.
     * @param Api $shopify
     * @param OAuthHelper $authHelper
     */
    public function __construct(Api $shopify,
                                OAuthHelper $authHelper,
                                ShopifyWebhooksService $shopifyWebhooksService) {

        $this->shopify = $shopify;
        $this->auth = $authHelper;
        $this->shopifyWebhooksService = $shopifyWebhooksService;
    }


    /**
     * @return bool
     */
    private function isValidRequest(): bool {

        $query = $this->request->all();

        if (!isset($query['timestamp'], $query['hmac'], $query['shop'])) {
            return false;
        }

        $seconds_in_a_day = 24 * 60 * 60;
        $older_than_a_day = $query['timestamp'] < (time() - $seconds_in_a_day);

        if ($older_than_a_day) {
            return false;
        }

        $hmac = $query['hmac'];

        $dataString = [];

        foreach ($query as $key => $value) {

            if ( $key === 'hmac' ) {
                continue;
            }

            $key = str_replace('=', '%3D', $key);
            $key = str_replace('&', '%26', $key);
            $key = str_replace('%', '%25', $key);
            $value = str_replace('&', '%26', $value);
            $value = str_replace('%', '%25', $value);
            $dataString[] = $key . '=' . $value;
        }
        sort($dataString);

        $string = implode("&", $dataString);

        return hash_hmac('sha256', $string, config('shopify.secret_key')) === $hmac;
    }


    /**
     * @param string $to
     * @return string
     */
    private function getOauthUrl(string $to) {

        $redirectUrl = url($to);
        $permissions = implode(',', config('shopify.permissions'));

        return $this->auth->getAuthorizationUrl($redirectUrl, $permissions, false);
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {

        $this->request = $request;

        $storeDomain = $this->request->get('shop');
        if( ! $storeDomain ) {
            return view('store-auth-view');
        }

        $this->shopify->setMyshopifyDomain($storeDomain);

        $oauthUrl = $this->getOauthUrl("/oauth");

        if ($this->isValidRequest()) {

            try {

                /**
                 * @var $store Store
                 */
                $store = Store::where('storeDomain', $storeDomain)->firstOrFail();
                if ($store->isActive()) {

                    $this->shopify->setAccessToken($store->storeKey);

                    $this->shopifyWebhooksService->checkRegisteredWebhooks();

                    $this->request->attributes->add([
                        'store' => $store
                    ]);

                    return $next($this->request);
                }

            } catch (\Exception $e) {}

            if (!$code = $this->request->input('code')) {

                return view('oauth-redirect-view', [
                    'redirect' => $oauthUrl
                ]);
            }

            return $next($this->request);
        }

        return redirect($oauthUrl);
    }
}
