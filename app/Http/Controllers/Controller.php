<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController {

    public function viewSuccess($data = []) {
        return view('index-view', [
            "data" => $data
        ]);
    }

    public function viewError($error) {
        return view('error-view', [
            "data" => $error
        ]);
    }

    /**
     *  @OA\Schema(
     *   schema="RequestSuccess",
     *   type="object",
     *   @OA\Property(property="code", format="int64", type="integer"),
     *   @OA\Property(property="message", type="string")
     * )
     */
    public function jsonSuccess($data = [], $message = null) {

        return response()->json([
            "code"    => 200,
            "message" => $message,
            "data"    => $data
        ]);
    }

}
