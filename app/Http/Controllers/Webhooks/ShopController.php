<?php

namespace App\Http\Controllers\Webhooks;

use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Utils\Shopify\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ShopController extends Controller {

    /**
     * @var Api
     */
    private $shopify;

    /**
     * @var Store
     */
    private $store;

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var array
     */
    private $data;

    public function __construct( Request $request, Api $shopify ) {
        $this->shopify = $shopify;
        $this->request = $request;
        $this->data = $this->request->get('data');
        $this->store = $this->request->get('store');
    }

    public function uninstalled() {

        try {

            $this->store->active = false;
            $this->store->save();

        } catch (\Exception $e) {
            Log::error( $e->getMessage() );
        }
    }
}
