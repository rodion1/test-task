<?php

namespace App\Http\Controllers\Shopify;

use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Utils\Shopify\Api;
use App\Utils\Shopify\Helper\OAuthHelper;
use Illuminate\Http\Request;

class OauthController extends Controller {

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;


    /**
     * @var Api
     */
    private $shopify;


    /**
     * @var Store
     */
    private $store;


    /**
     * @var OAuthHelper
     */
    private $auth;


    /**
     * @var string
     */
    private $storeDomain;


    /**
     * OauthController constructor.
     * @param Request $request
     * @param Api $shopify
     * @param OAuthHelper $authHelper
     */
    public function __construct(Request $request,
                                Api $shopify,
                                OAuthHelper $authHelper) {

        $this->request = $request;
        $this->shopify = $shopify;
        $this->auth = $authHelper;

        $this->store = $request->get('store');
        $this->storeDomain = $this->shopify->getMyshopifyDomain();
    }


    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View|\Laravel\Lumen\Http\Redirector
     */
    public function index() {

        if ($code = $this->request->input('code')) {
            return $this->install($code);
        }

        return redirect("https://{$this->storeDomain}/admin/apps/{$this->shopify->getApiKey()}");
    }


    /**
     * @param string $code
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View|\Laravel\Lumen\Http\Redirector
     */
    private function install(string $code) {

        try {

            $token = $this->auth->getAccessToken($code);

            Store::updateOrCreate(['storeDomain' => $this->storeDomain], [
                'storeKey' => $token->access_token,
                'active'   => true
            ]);

            $this->shopify->setAccessToken($token->access_token);

        } catch (\Exception $e) {

            return $this->viewError([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ]);
        }

        return redirect("https://{$this->storeDomain}/admin/apps/{$this->shopify->getApiKey()}");
    }

}
