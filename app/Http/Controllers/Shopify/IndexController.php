<?php

namespace App\Http\Controllers\Shopify;

use App\Models\Store;
use App\Utils\Shopify\Api;
use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;

class IndexController extends Controller {

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;


    /**
     * @var Api
     */
    private $shopify;


    /**
     * @var Store
     */
    private $store;


    /**
     * IndexController constructor.
     * @param Request $request
     * @param Api $shopify
     * @param ProjectSettingsService $settingsService
     */
    public function __construct(Request $request,
                                Api $shopify) {

        $this->request = $request;
        $this->shopify = $shopify;

        $this->store = $request->get('store');
    }


    /**
     * @return \Illuminate\View\View
     */
    public function index() {

        return $this->viewSuccess();
    }
}
