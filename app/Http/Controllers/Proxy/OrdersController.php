<?php

namespace App\Http\Controllers\Proxy;

use App\Services\IpFindService;
use App\Services\ShopifyOrdersService;
use \App\Http\Controllers\Controller;

class OrdersController extends Controller
{


    /**
     * @var ShopifyOrdersService
     */
    private $shopifyOrdersService;


    /**
     * @var IpFindService
     */
    private $ipFindService;

    /**
     * RedirectController constructor.
     * @param ShopifyOrdersService $shopifyOrdersService
     * @param IpFindService $ipFindService
     */
    public function __construct(ShopifyOrdersService $shopifyOrdersService, IpFindService $ipFindService)
    {
        $this->shopifyOrdersService = $shopifyOrdersService;
        $this->ipFindService = $ipFindService;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
     */
    public function list()
    {

        $userCountry          = $this->ipFindService->getCountryByIp();
        $countAll             = $this->shopifyOrdersService->getCountAll();
        $countByCountry       = $this->shopifyOrdersService->getCountByCountry($userCountry);
        $countryOrdersPercent = !empty($countAll) ? ($countByCountry / $countAll) * 100 : 0;

        if ($countryOrdersPercent != 100)
        {
            $countryOrdersPercent = number_format($countryOrdersPercent, 2, '.', '');
        }

        return view('country-order-stats-view', [
            'data' => [
                'userCountry' => $userCountry,
                'countAll' => $countAll,
                'countByCountry' => $countByCountry,
                'countryOrdersPercent' => $countryOrdersPercent
            ]
        ]);
    }
}
