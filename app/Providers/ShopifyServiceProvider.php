<?php

namespace App\Providers;

use App\Utils\Shopify\Api;
use App\Utils\Shopify\Helper\OAuthHelper;
use Laravel\Lumen\Application;
use Illuminate\Support\ServiceProvider;

class ShopifyServiceProvider extends ServiceProvider {


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {

        $this->app->singleton(Api::class, function (Application $app) {
            return new Api([
                'api_key'    => config('shopify.api_key'),
                'api_secret' => config('shopify.secret_key')
            ]);
        });


        $this->app->singleton(OAuthHelper::class, function ( Application $app) {
            return $app->make(Api::class)->getOAuthHelper();
        });
    }
}
