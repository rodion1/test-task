<?php

namespace App\Providers;

use App\Services\ShopifyOrdersService;
use App\Services\ShopifyWebhooksService;
use App\Utils\Shopify\Api;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

class AppServiceProvider extends ServiceProvider {

    public function boot() {
        app('url')->forceRootUrl(config('app.url'));
        app('url')->forceSchema('https');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {

        $this->app->singleton(ShopifyWebhooksService::class, function (Application $app) {
            return new ShopifyWebhooksService($app->make(Api::class));
        });


        $this->app->singleton(ShopifyOrdersService::class, function (Application $app) {
            return new ShopifyOrdersService($app->make(Api::class));
        });
    }
}
