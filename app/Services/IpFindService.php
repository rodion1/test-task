<?php

namespace App\Services;

use GuzzleHttp\Client;

class IpFindService
{

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getCountryByIp()
    {
        $user_ip = getenv('HTTP_CLIENT_IP') ?:
            getenv('HTTP_X_FORWARDED_FOR') ?:
                getenv('HTTP_X_FORWARDED') ?:
                    getenv('HTTP_FORWARDED_FOR') ?:
                        getenv('HTTP_FORWARDED') ?:
                            getenv('REMOTE_ADDR');

        $user_ip = explode(",", $user_ip)[0];
        $ip_find_api_key = env('IP_FIND_API_KEY');

        try {
            $result = $this->client->get("https://ipfind.co", [
                'query' => [
                    'ip' => $user_ip,
                    'auth' => $ip_find_api_key
                ]
            ]);
        } catch (\Exception $e) {
            return null;
        }

        $data = json_decode($result->getBody()->getContents());
        if (empty($data)) {
            return null;
        }

        return $data->country;
    }
}