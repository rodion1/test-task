<?php

namespace App\Services;

use App\Utils\Shopify\Api;
use App\Utils\Shopify\Service\OrderService;

class ShopifyOrdersService {


    /**
     * @var Api
     */
    private $shopify;


    /**
     * @var OrderService
     */
    private $orderService;


    /**
     * ShopifyWebhooksService constructor.
     * @param Api $shopify
     */
    public function __construct(Api $shopify) {
        $this->shopify = $shopify;
        $this->orderService = new OrderService($this->shopify);
    }

    /**
     * @return int
     */
    public function getCountAll() {
        return $this->orderService->count();
    }

    /**
     * @return \App\Utils\Shopify\Object\Order[]
     */
    public function getOrders() {
        $orders = $this->orderService->all();
        return $orders;
    }

    /**
     * @param $country
     * @return int
     */
    public function getCountByCountry($country) {

        $orders  = $this->getOrders();
        $country = strtolower($country);
        $count = 0;

        foreach ($orders AS $order){
            $orderCountry = strtolower($order->billing_address->country);
            if ($orderCountry == $country)
            {
                $count++;
            }
        }

        return $count;
    }

}

