<?php

namespace App\Services;

use App\Utils\Shopify\Api;
use App\Utils\Shopify\Object\Webhook;
use App\Utils\Shopify\Service\WebhookService;
use Illuminate\Support\Facades\Log;

class ShopifyWebhooksService {


    /**
     * @var array
     */
    private $webhooks = [
        "app/uninstalled"
    ];


    /**
     * @var Api
     */
    private $shopify;


    /**
     * ShopifyWebhooksService constructor.
     * @param Api $shopify
     */
    public function __construct(Api $shopify) {
        $this->shopify = $shopify;
    }


    /**
     * @param string $topic
     * @return string
     */
    private function getWebhookAddress(string $topic): string {
        return url("webhooks/{$topic}");
    }


    public function checkRegisteredWebhooks(): void {

        $webhooksService = new WebhookService($this->shopify);
        $webhooks = array_filter($webhooksService->all(), function (Webhook $webhook) use (&$webhooksService){

            if( ! in_array($webhook->topic, $this->webhooks) ) {

                $webhooksService->delete($webhook);

                return false;
            }

            if( $webhook->address !== $this->getWebhookAddress($webhook->topic) ) {

                $webhooksService->delete($webhook);

                return false;
            }

            return true;
        });

        foreach ($this->webhooks as $topic) {

            if (count($this->findWebhooks($webhooks, $topic)) > 0) {
                continue;
            }

            $this->registerWebhook($webhooksService, $topic);
        }

    }


    /**
     * @param array $webhooks
     * @param string $topic
     * @return array
     */
    private function findWebhooks(array &$webhooks, string $topic): array {

        return array_filter($webhooks, function (Webhook $webhook) use (&$webhooksService, $topic) {
            return $webhook->topic == $topic;
        });
    }


    /**
     * @param WebhookService $webhooksService
     * @param string $topic
     */
    private function registerWebhook(WebhookService &$webhooksService, string $topic): void {

        $webhook = new Webhook();
        $webhook->topic = $topic;
        $webhook->address = $this->getWebhookAddress($topic);
        $webhook->format = "json";

        try {
            $webhooksService->create($webhook);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

}

