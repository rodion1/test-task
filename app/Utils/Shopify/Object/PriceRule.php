<?php

namespace App\Utils\Shopify\Object;

use \App\Utils\Shopify\Enum\Fields\PriceRuleFields;

class PriceRule extends AbstractObject
{
    public static function getFieldsEnum()
    {
        return PriceRuleFields::getInstance();
    }
}
