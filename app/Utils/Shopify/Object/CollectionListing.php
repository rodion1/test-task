<?php

namespace App\Utils\Shopify\Object;

use \App\Utils\Shopify\Enum\Fields\CollectionListingFields;

class CollectionListing extends AbstractObject
{
    public static function getFieldsEnum()
    {
        return CollectionListingFields::getInstance();
    }
}
