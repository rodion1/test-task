<?php

namespace App\Utils\Shopify\Object;

use \App\Utils\Shopify\Enum\Fields\CustomerInviteFields;

class CustomerInvite extends AbstractObject
{
    public static function getFieldsEnum()
    {
        return CustomerInviteFields::getInstance();
    }
}
