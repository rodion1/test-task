<?php

namespace App\Utils\Shopify\Object;

use \App\Utils\Shopify\Enum\Fields\ShippingLineFields;

class ShippingLine extends AbstractObject
{
    public static function getFieldsEnum()
    {
        return ShippingLineFields::getInstance();
    }
}
