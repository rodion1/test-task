<?php

namespace App\Utils\Shopify\Object;

use \App\Utils\Shopify\Enum\Fields\CheckoutFields;

class Checkout extends AbstractObject
{
    public static function getFieldsEnum()
    {
        return CheckoutFields::getInstance();
    }
}
