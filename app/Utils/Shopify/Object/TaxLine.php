<?php

namespace App\Utils\Shopify\Object;

use \App\Utils\Shopify\Enum\Fields\TaxLineFields;

class TaxLine extends AbstractObject
{
    public static function getFieldsEnum()
    {
        return TaxLineFields::getInstance();
    }
}
