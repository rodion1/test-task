<?php

namespace App\Utils\Shopify;

interface ApiInterface
{
    public function getHttpHandler();
    public function getMyshopifyDomain();
}
