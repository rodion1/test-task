<?php

namespace App\Utils\Shopify\Service;

use \App\Utils\Shopify\Object\Asset;
use \App\Utils\Shopify\Exception\ShopifySdkException;

class AssetService extends AbstractService
{
    /**
     * Receive a list of all assets
     *
     * @link   https://help.shopify.com/api/reference/asset#index
     * @param  integer $themeId
     * @param  array   $params
     * @return Asset[]
     */
    public function all($themeId, array $params = array())
    {
        $endpoint = '/admin/themes/'.$themeId.'/assets.json';
        $response = $this->request($endpoint, 'GET', $params);
        return $this->createCollection(Asset::class, $response['assets']);
    }

    /**
     * Receive a single asset
     *
     * @link   https://help.shopify.com/api/reference/asset#show
     * @param  integer $themeId
     * @param  array   $params
     * @return Asset
     */
    public function get($themeId, array $params = array())
    {
        $endpoint = '/admin/themes/'.$themeId.'/assets.json';
        $response = $this->request($endpoint, 'GET', $params);
        return $this->createObject(Asset::class, $response['asset']);
    }

    /**
     * Creating or modifying an asset
     *
     * @link   https://help.shopify.com/api/reference/asset#update
     * @param  integer $themeId
     * @param  Asset   $asset
     * @return void
     */
    public function put($themeId, Asset &$asset)
    {
        $data = $asset->exportData();
        $endpoint= '/admin/themes/'.$themeId.'/assets.json';
        $response = $this->request(
            $endpoint, 'PUT', array(
                'asset' => $data
            )
        );
        $asset->setData($response['asset']);
    }

    /**
     * Remove an asset from the database
     *
     * @link   https://help.shopify.com/api/reference/asset#destroy
     * @param  integer $themeId
     * @param  Asset   $asset
     * @return void
     */
    public function delete($themeId, Asset $asset)
    {
        $endpoint = '/admin/themes/'.$themeId.'/assets.json';
        $this->request($endpoint, 'DELETE', [
            'asset' => [
                'key' => $asset->key
            ]
        ]);
        return;
    }
}
