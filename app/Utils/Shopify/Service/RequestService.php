<?php

namespace App\Utils\Shopify\Service;

class RequestService extends AbstractService {

    public function get($endpoint, array $params = array()) {
        return $this->request($endpoint, 'GET', $params);
    }

    public function post($endpoint, array $params = array()) {
        return $this->request($endpoint, 'POST', $params);
    }

    public function put($endpoint, array $params = array()) {
        return $this->request($endpoint, 'PUT', $params);
    }

    public function delete($endpoint, array $params = array()) {
        return $this->request($endpoint, 'DELETE', $params);
    }
}
