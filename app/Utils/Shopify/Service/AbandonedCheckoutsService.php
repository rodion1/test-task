<?php

namespace App\Utils\Shopify\Service;

use \App\Utils\Shopify\Object\AbandonedCheckout;

class AbandonedCheckoutsService extends AbstractService
{
    /**
     * List all abandonded checkouts
     *
     * @link   https://help.shopify.com/api/reference/abandoned_checkouts#index
     * @param  array $params
     * @return AbandonedCheckout[]
     */
    public function all(array $params = array())
    {
        $data = $this->request('/admin/checkouts.json', 'GET', $params);
        return $this->createCollection(AbandonedCheckout::class, $data['checkouts']);
    }

    /**
     * Get a count of checkouts
     *
     * @link   https://help.shopify.com/api/reference/abandoned_checkouts#count
     * @param  array $params
     * @return integer
     */
    public function count(array $params = array())
    {
        $data = $this->request('/admin/checkouts/count.json', 'GET', $params);
        return $data['count'];
    }
}
