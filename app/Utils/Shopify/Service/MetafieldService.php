<?php

namespace App\Utils\Shopify\Service;

use \App\Utils\Shopify\Object\Metafield;

class MetafieldService extends AbstractService {

    public function all(array $params = array()) {
        $data = $this->request('/admin/metafields.json', 'GET', $params);
        return $this->createCollection(Metafield::class, $data['metafields']);
    }


    public function count(array $params = array()) {
        $data = $this->request('/admin/metafields/count.json', 'GET', $params);
        return $data['count'];
    }


    public function get($metafieldId, array $params = array()) {
        $data = $this->request('/admin/metafields/' . $metafieldId . '.json', 'GET', $params);
        return $this->createObject(Metafield::class, $data['metafield']);
    }

    public function search(array $condition) {

        $data = $this->request('/admin/metafields.json', 'GET', $condition);

        if( ! empty($data['metafields'][0])) {
            return $this->createObject(Metafield::class, $data['metafields'][0]);
        }

        return null;
    }

    public function create(Metafield &$metafield) {
        $data = $metafield->exportData();
        $response = $this->request(
            '/admin/metafields.json', 'POST', array(
                'metafield' => $data
            )
        );
        $metafield->setData($response['metafield']);
    }

    public function createOrRemove(Metafield $metafield) {

        $data = $metafield->exportData();

        if( empty($data['value']) ) {

            $condition = [
                'key' => $data['key'],
                'namespace' => $data['namespace']
            ];

            if( ! empty($data['owner_resource']) ) {
                $condition['metafield']['owner_resource'] = $data['owner_resource'];
            }

            if( ! empty($data['owner_id']) ) {
                $condition['metafield']['owner_id'] = $data['owner_id'];
            }

            if( $searchedMeta = $this->search($condition) ) {
                $this->delete( $searchedMeta );
            }

            $metafield = null;

        } else {
            $response = $this->request(
                '/admin/metafields.json', 'POST', array(
                    'metafield' => $data
                )
            );

            $metafield->setData($response['metafield']);
        }
    }

    public function update(Metafield $metafield) {
        $data = $metafield->exportData();
        $response = $this->request(
            '/admin/metafields/' . $metafield->id . '.json', 'PUT', array(
                'metafield' => $data
            )
        );
        $metafield->setData($response['metafield']);
    }


    public function delete(Metafield $metafield) {
        $this->request('/admin/metafields/' . $metafield->id . '.json', 'DELETE');
    }
}
