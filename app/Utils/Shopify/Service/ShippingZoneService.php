<?php

namespace App\Utils\Shopify\Service;

use \App\Utils\Shopify\Object\ShippingZone;
use \App\Utils\Shopify\Exception\ShopifySdkException;

class ShippingZoneService extends AbstractService
{
    /**
     * Return a list of shipping zones
     *
     * @link   https://help.shopify.com/api/reference/shipping_zone#index
     * @return ShippingZone[]
     */
    public function all()
    {
        throw new ShopifySdkException("ShippingZoneService not implemented");
    }
}
