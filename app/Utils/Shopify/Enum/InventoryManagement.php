<?php

namespace App\Utils\Shopify\Enum;

class InventoryManagement
{
    const BLANK = 'blank';
    const SHOPIFY = 'shopify';
}
