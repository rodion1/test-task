<?php

namespace App\Utils\Shopify\Enum;

class OrderFulfillmentStatus
{
    const FULFILLED = 'fulfilles';
    const NULL = 'null';
    const PARTIAL = 'partial';
}
