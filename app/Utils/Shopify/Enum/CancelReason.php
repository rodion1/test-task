<?php

namespace App\Utils\Shopify\Enum;

class CancelReason
{
    const CUSTOMER = 'customer';
    const FRAUD = 'fraud';
    const INVENTORY = 'inventory';
    const OTHER = 'other';
}
