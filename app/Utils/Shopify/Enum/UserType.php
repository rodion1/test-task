<?php

namespace App\Utils\Shopify\Enum;

class UserType
{
    const REGULAR = 'regular';
    const OPEN_ID = 'open_id';
    const RESTRICTED = 'restricted';
}
