<?php

namespace App\Utils\Shopify\Enum;

class ThemeRole
{
    const MAIN = 'main';
    const UNPUBLISHED = 'unpublished';
}
