<?php

namespace App\Utils\Shopify\Enum;

class InventoryPolicy
{
    const DENY = 'deny';
    const CONTINUE = 'continue';
}
