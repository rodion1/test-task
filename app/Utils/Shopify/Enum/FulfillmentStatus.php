<?php

namespace App\Utils\Shopify\Enum;

class FulfillmentStatus
{
    const PENDING = 'pending';
    const OPEN = 'open';
    const SUCCESS = 'success';
    const CANCELLED = 'cancelled';
    const ERROR = 'error';
    const FAILURE = 'failure';
}
