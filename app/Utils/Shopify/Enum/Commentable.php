<?php

namespace App\Utils\Shopify\Enum;

class Commentable
{
    const NO = 'no';
    const MODERATE = 'moderate';
    const YES = 'yes';
}
