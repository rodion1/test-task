<?php

namespace App\Utils\Shopify\Enum;

class OrderRiskRecommendation
{
    const CANCEL = 'cancel';
    const INVESTIGATE = 'investigate';
    const ACCEPT = 'accept';
}
