<?php

namespace App\Utils\Shopify\Enum\Fields;

abstract class AbstractObjectEnum
{
    public abstract function getFieldTypes();

    public function getFields()
    {
        $enum = $this->getFieldTypes();

        return array_keys($enum);
    }

    public static function getInstance()
    {
        return new static();
    }
}
