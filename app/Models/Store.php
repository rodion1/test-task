<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model {

    public $table = 'stores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'storeDomain',
        'storeKey',
        'active'
    ];

    protected $hidden = [
        'id',
        'updated_at',
        'created_at',
        'active'
    ];

    public function isActive() {
        return ! empty( $this->attributes['active'] );
    }
}